# M&S Test


## 🚀 Quick start

**Download the repository.**

  Get the latest files from the repository

  ```sh
  # Download the project on your development machine on your preferred location
  git clone https://punalchotrani@bitbucket.org/punalchotrani/nodesandbox.git
  ```

**Start developing.**

  Navigate into the project’s directory and start it up.

  ```sh
  cd node-sandbox
  npm i
  npm start
  ```
  

**Open the source code and start editing!**

  Your site is now running at `http://localhost:8080`


## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in this project.

    ├── app
    ├── node_modules
    ├── public
    ├── sass
    ├── package-lock.json
    ├── package.json
    └── README.md

## 🔧 Tasks to be completed
**Unit testing**

**Frontend JS** 

**End to End testing**

**Styling**

// require express
var express = require('express');
var path = require('path');
const axios = require('axios');

// create our router object
var router = express.Router();

// export our router
module.exports = router;


// route for our home page
router.get('/', (req, res) => {
	axios
		.get(`https://interview-tech-testing.herokuapp.com/product-details/p60211771`, {
			auth: {
				username: 'admin',
				password: 'password'
			}
		})
		.then(response => {
			res.render('./pages/index', {
				data: response.data,
				styles: response.data.styles
			});
			console.log(response.data.styles[0].images[0]);
		})
		.catch(error => console.log(error));
});